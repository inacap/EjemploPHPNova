<?php

namespace App\Models;

use Nova\Database\ORM\Model;

class UsuariosModel extends Model
{
  protected $table = 'usuarios';

  /**
   * @var array
   */
  protected $fillable = array(
    'username',
    'password',
    'email',
    'updated_at',
    'created_at'
  );
}
