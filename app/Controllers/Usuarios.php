<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\UsuariosModel;
use Nova\Http\Request;
use Nova\Support\Facades\Response;
use Nova\Support\Facades\View;


class Usuarios extends BaseController
{
    public function index()
    {
      $usuarios = UsuariosModel::all();
      return Response::json($usuarios, 200);
    }

    public function store(Request $request)
    {
      $username = $request->input('username');
      $password = $request->input('password');
      $email = $request->input('email');

      $usuario = UsuariosModel::create(array(
          'username'   => $username,
          'password'   => $password,
          'email'      => $email
      ));

      $usuario->save();
      return Response::json(array("status"=> "ok"), 200);
    }

    public function remove(Request $request, $id)
    {
      $usuario = UsuariosModel::find($id);
      $usuario->delete();

      return Response::json(array("status"=> "ok"), 200);
    }

    public function update(Request $request, $id)
    {
      $username = $request->input('username');
      $password = $request->input('password');
      $email = $request->input('email');

      $usuario = UsuariosModel::find($id);
      $usuario->username = $username;
      $usuario->password = $password;
      $usuario->email = $email;
      $usuario->save();

      return Response::json(array("status"=> "ok"), 200);
    }

    public function indexView()
    {
      return View::make('Usuarios/Index')->shares('title', "Lista usuarios");
    }

    public function storeView()
    {
      return View::make('Usuarios/Store')->shares('title', "Crear usuario");
    }

}
