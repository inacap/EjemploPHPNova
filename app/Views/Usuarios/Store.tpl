<h1>Agregar usuario</h1>
<form id="formulario">
  <div class="form-group">
    <label for="username">Username:</label>
    <input type="text" class="form-control" id="username">
  </div>
  <div class="form-group">
    <label for="pwd">Password:</label>
    <input type="password" class="form-control" id="password">
  </div>
  <div class="form-group">
    <label for="email">Email:</label>
    <input type="text" class="form-control" id="email">
  </div>
  <button type="submit" class="btn btn-primary">Crear</button>
  <a href="../usuarios" class="btn btn-default">Cancelar</a>
</form>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
      $("#formulario").submit(function(e){
        e.preventDefault();

        var usuarioNuevo = {
          username: $('#username').val(),
          password: $('#password').val(),
          email: $('#email').val()
        };

        var usuarioData = JSON.stringify(usuarioNuevo);

        $.ajax({
            type: 'POST',
            url:  '../api/usuarios',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data: usuarioData,
            //
            success: function (data) {
                alert(JSON.stringify(data));
            },
            error: function (data) {
                console.log('An error occurred.');

                console.log(data);
            },
        });
      });

    });
</script>
