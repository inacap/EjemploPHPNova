<h1>Lista usuarios</h1>
<a href="usuarios/crear" class="btn btn-primary pull-right mb-10">Crear usuario</a>
<table id="tabla_usuarios" class="table table-striped table-hover responsive">
  <tr class="bg-navy disabled">
      <th style="text-align: center; vertical-align: middle;">ID</th>
      <th style="text-align: center; vertical-align: middle;">Nombre</th>
      <th style="text-align: center; vertical-align: middle;">Email</th>
      <th style="text-align: center; vertical-align: middle;">Editar</th>
      <th style="text-align: center; vertical-align: middle;">Eliminar</th>
  </tr>
</table>

<!-- Modal Eliminar -->
<div id="modalEliminar" data-id="-1" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">¿Desea eliminar este usuario?</h4>
      </div>
      <div class="modal-body">
        <p>Esta acción no podrá deshacerse, ya no podrá usar estos datos.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="eliminar()">Eliminar</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal Editar -->
<div id="modalEditar" data-id="-1" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Editar Usuario</h4>
      </div>
      <div class="modal-body">
        <form id="formularioEditar">
          <div class="form-group">
            <label for="username">Username:</label>
            <input type="text" class="form-control" id="editarUsername">
          </div>
          <div class="form-group">
            <label for="pwd">Password:</label>
            <input type="password" class="form-control" id="editarPassword">
          </div>
          <div class="form-group">
            <label for="email">Email:</label>
            <input type="text" class="form-control" id="editarEmail">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-success" data-dismiss="modal" onclick="editar()">Editar</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript" charset="utf-8">
    var usuarios;
    $(document).ready(function() {
      $.ajax({
          type: 'GET',
          url:  'api/usuarios',
          dataType: 'json',

          //
          success: function (data) {
              usuarios = data;
              for(var i = 0; i < data.length; i++){
                $("#tabla_usuarios").append(
                  '<tr class="bg-navy disabled">' +
                  '<td style="text-align: center; vertical-align: middle;">' + data[i]['id'] + '</td>'+
                  '<td style="text-align: center; vertical-align: middle;">'+ data[i]['username'] + '</td>'+
                  '<td style="text-align: center; vertical-align: middle;">'+ data[i]['email'] + '</td>'+
                  '<td style="text-align: center; vertical-align: middle;"><a href="javascript:void(0);" data-toggle="modal" data-target="#modalEditar" data-id="' + data[i]['id'] + '" class="btn btn-warning" onclick="onModalEditar(this)">Editar</a></td>'+
                  '<td style="text-align: center; vertical-align: middle;"><a href="javascript:void(0);" data-toggle="modal" data-target="#modalEliminar" data-id="' + data[i]['id'] + '" class="btn btn-danger btn-eliminar" onclick="onModalEliminar(this)">Eliminar</a></td>'+
                  '</tr>');
              }
          },
          error: function (data) {
              console.log('An error occurred.');

              console.log(data);
          },
      });
    });

    function onModalEliminar(d){
        $('#modalEliminar').data('id',d.getAttribute("data-id"));
    }

    function eliminar(d){
        var id = $('#modalEliminar').data('id');

        $.ajax({
            type: 'DELETE',
            url:  'api/usuarios/' + id,
            dataType: 'json',

            //
            success: function (data) {
                location.reload();
            },
            error: function (data) {
                console.log('An error occurred.');

                console.log(data);
            },
        });
    }

    function onModalEditar(d){
      $('#modalEditar').data('id',d.getAttribute("data-id"));
      for(var i = 0; i < usuarios.length; i++){
        if(usuarios[i]["id"] == d.getAttribute("data-id")){
          $("#editarUsername").val(usuarios[i]["username"]);
          $("#editarPassword").val(usuarios[i]["password"]);
          $("#editarEmail").val(usuarios[i]["email"]);
          break;
        }
      }
    }

    function editar(d){
        var id = $('#modalEditar').data('id');
        var usuarioEditado = {
          username: $('#editarUsername').val(),
          password: $('#editarPassword').val(),
          email: $('#editarEmail').val()
        };

        var usuarioData = JSON.stringify(usuarioEditado);

        $.ajax({
            type: 'PUT',
            url:  'api/usuarios/' + id,
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data: usuarioData,

            //
            success: function (data) {
                location.reload();
            },
            error: function (data) {
                console.log('An error occurred.');

                console.log(data);
            },
        });
    }
</script>
